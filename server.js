'use strict'

const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');
const Handlebars = require('handlebars');
const Routes = require('./http/routes.js');

const server = new Hapi.Server({
		port: 3000,
		host: '0.0.0.0'
	});

const startServer = async () => {

    await server.register(Inert);
    await server.register(Vision);
    await server.register(Routes);

    server.views({
    	engines: { html: Handlebars },
    	path: 'views',
    	partialsPath: 'views/partials',
    	isCached: false // Set true for production!
    });

    await server.start();

    console.log('Server running at:', server.info.uri);
};

startServer();