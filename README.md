## Energy Bills Viewer ##

Webapp will retrieve a list of energy bills and display them in a sortable table.  Bills data is cached on the client for 30 seconds and on the backend for one minute.  

Backend is developed using Hapi v17 and consists of the server, routes, and a controller.  Inert, Vision, and Wreck are Hapi components providing static directory handling, template rendering, and an HTTP utility.

Frontend is vanilla JS.  Handlebars are used for templating.  Initial view from server includes initital data injected into the page through Handlebars.  Bills data is populated into a table through a pre-compiled Handlebars template on the client.  A third-party library enables sorting of the table on the header columns. 

**/**

**server**.js
Sets up and starts the server.  Routes and plugins are registered to the server.  Server's views are also setup to allow serving Handlebar pages and partials. 

**/http**

**routes**.js
Routes to the server are defined.  A public directory is declared to allow for assets to be retrieved as well as a route for the site index.  An api route for 'bills' is also created.  The api route is configured to allow client-side caching of the data for a total amount of time of 'clientCacheMaxAge'.  'createCache' in Controller is also invoked to setup server-side caching.

**controller**.js
Apart from the public directory, all routes are handled in the controller.  'getIndex' returns a Handlebars view.  'getBills' returns an array of bills from 'getBillsURI'.  Server side cache is also set up within controller to prevent too many hits to 'getBillsURI'.


**/public/scripts**
'main.js' initializes click events for the buttons.  Also fetches bills from the server and displays the table using a Handlebars template.  An inner class 'Utils' provides a quick method to get elements by ID. 

**/public/lib**
Scripts that are third-party.  The script 'sorttable' provides sorting on a table without added dependencies. 

**/public/styles**
A single CSS sheet is used. 

**/public/templates**
Pre-compiled Handlebars template for client use. 

**views**
Views to be returned by the server.  Partials are stored here as well.  'billsGrid' in /partials is compiled in public/templates.


### To Run ###
Node v8 (or higher) is required. 

From '/': 

1.  Run "npm install".
2.  Run "node server".
3.  Visit "http://localhost:3000/" in your browser*. 

*Tested on Chrome and FF...  Runs as expected.  Mobile as well, though landscape mode is recommended. 