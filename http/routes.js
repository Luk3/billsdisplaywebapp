'use strict'

const Controller = require('./controller.js');

const clientCacheMaxAge = 30 * 1000;

exports.name = 'routes';

exports.register = function(server, options) {

	Controller.createCache(server);

	server.route({
		method: 'GET',
		path: '/{param*}',
		config: {
			handler: {
				directory: {
					path: 'public'
				}
			}
		}
	});

	server.route({
		method: 'GET',
		path: '/',
		handler: Controller.getIndex
	});

	server.route({
		method: 'GET',
		path: '/api/v1/bills',
		config: {
			cache: {
				expiresIn: clientCacheMaxAge, 
				privacy: 'private'
			}
		},
		handler: server.methods.getBills
	});
}