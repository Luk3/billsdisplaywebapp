'use strict'

const Wreck = require('wreck');

const pageTitle = 'Energy Bills Viewer';
const indexInitContent = '<h1>Use sidebar to get electric use!<h1>';
const getBillsURI = 'https://snapmeter.com/api/v2/531e19848df5cb0b35014e85/meters/2166484536790/bill-summary?token=6d547442-417b-41a3-8838-b022f9c2974d';

const serverCacheExpiresIn = 100 * 1000; 
const serverTimeout = 3000; 

module.exports = {
	getIndex: function(request, h) {
			return h.view('index', {
				title: pageTitle,
				content: indexInitContent
			});
	},
	getBills: async function(request, h) {
		const {res, payload} = await Wreck.get(getBillsURI);
		return JSON.parse(payload);
	},
	createCache: function(server) {
		server.method('getBills', this.getBills, {
			cache: {
				expiresIn: serverCacheExpiresIn, 
				generateTimeout: serverTimeout
			},
			generateKey: function(request){
				return JSON.stringify(request.query);
			}
		});
	}
};