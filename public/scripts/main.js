
const mainContent = '<h1>Use sidebar to get electric use!<h1>';

document.addEventListener("DOMContentLoaded", function(){
  	init();
});

function init() {
	initEventListeners();
}

function initEventListeners() {
	Utils.getElementById("viewBillsButton").addEventListener("click", displayBills);

	Utils.getElementById("clearButton").addEventListener("click", resetMainDiv);
}

function displayBills() {
	fetch('/api/v1/bills').then((resp) => resp.json())
	.then(function(data) {
		var html = Handlebars.templates['billsGrid'](data);
		Utils.getElementById("main").innerHTML = html;
		sorttable.makeSortable(document.getElementsByTagName("TABLE")[0]);
	});
}

function resetMainDiv() {
	Utils.getElementById("main").innerHTML = mainContent;
}


class Utils {
	static getElementById(elem) {
		return document.getElementById(elem);
	}
}