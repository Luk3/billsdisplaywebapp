(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['billsGrid'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "	<tr>\r\n		<td> "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.attributes : depth0)) != null ? stack1.initial : stack1), depth0))
    + " </td>\r\n		<td> "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.attributes : depth0)) != null ? stack1.closing : stack1), depth0))
    + " </td>\r\n		<td> $"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.attributes : depth0)) != null ? stack1.cost : stack1), depth0))
    + " </td>\r\n		<td> "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.attributes : depth0)) != null ? stack1.peak : stack1), depth0))
    + " kW </td>\r\n		<td> "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.attributes : depth0)) != null ? stack1.used : stack1), depth0))
    + " kW </td>\r\n	</tr>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "\r\n<table class=\"sortable\">\r\n	<tr>\r\n		<th> From </th>\r\n		<th> To </th>\r\n		<th> Cost </th>\r\n		<th> Peak kW </th>\r\n		<th> Total kW </th>\r\n	</tr>\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.data : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</table>";
},"useData":true});
})();